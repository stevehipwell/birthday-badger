$ErrorActionPreference = "Stop"

dotnet publish -c Release -r linux-arm -o ../../dist

scp -r ./dist pi@raspberrypi.home:/home/pi/birthday-badger/

ssh pi@raspberrypi.home '/home/pi/birthday-badger/birthday-badger'
